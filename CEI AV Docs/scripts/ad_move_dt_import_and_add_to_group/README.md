# Usage of Add AD Group powershell script

## Description

This script grabs all users in the specified OUs and adds them to the the specified group(s).
If DT Import is one of the included OUs, it will also apply the student company to all users
in the DT Import folder and move them to the specified latest student OU.
AD configuration can be altered in the accompanying config.xml file.
Powershell must be run as an administrator that possesses AD admin priviledges.

Currently only adds users to "EC-Prod" that are not already part of the group, but can be expanded to include any number of groups.

This script is running every hour as a scheduled task on the server.
Log files containing every change are saved in this folder with timestamps.

## Manual Usage

Open powershell and navigate to the folder containing the "add_ad_group.ps1" script.  
Modify the "config.xml" file as is appropriate.  
Run the "add_ad_group.ps1" script.

## Table of Contents

- add_ad_group.ps1 -- Powershell script
- config.xml -- AD configuration settings
- README.md -- This README

## XML Structure
```xml
<config>		  -- Root node
  <root></root>		  -- AD root path information (DC=,DC=)
  <server></server>	  -- AD server location
  <groups>		  -- AD groups root node
    <group name="" />	  -- AD groups to add users to (name=administration)
  </groups>
  <output></output>	  -- Output path for logs and csv output (\log.txt)
  <latestOU></latestOU>	  -- Default OU to move new users (import folder specified in script)
  <ous>			  -- OU root node
    <ou name="" />	  -- OUs to check for users that are missing groups in groups node
  </ous>
</config>
```  

## Contributors  
Aaron Johnson  
Last Edit: 4/14/2022