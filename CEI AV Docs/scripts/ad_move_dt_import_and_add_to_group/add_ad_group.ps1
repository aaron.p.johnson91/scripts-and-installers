#
# Import AD Module and Commands
#
Import-Module ActiveDirectory
Get-Command -Module ActiveDirectory
Clear-Host

#
# Setup Config Vars
#
try {
	$configPath = Get-Location
	Write-host "Attempting to read config from $configPath\config.xml" -BackgroundColor Black -ForegroundColor Yellow
	[xml]$xmlConfigs = Get-Content -Path "$configPath\config.xml"

	$DATE = get-date -format yyyy-MM-dd

	$ROOT = $xmlConfigs.config.root
	$SERVER = $xmlConfigs.config.server
	$OUTPUT_PATH = "$configPath" + $xmlConfigs.config.output + "-$DATE.log"
	$LATEST_OU = $xmlConfigs.config.latestOU

	Write-host "Loaded the following config..." -BackgroundColor Black -ForegroundColor Green
	Write-host "AD Root: $ROOT" -BackgroundColor Black -ForegroundColor Green
	Write-host "AD Server: $SERVER" -BackgroundColor Black -ForegroundColor Green
	Write-host "Output path: $OUTPUT_PATH" -BackgroundColor Black -ForegroundColor Green
	Write-host "Latest OU: $LATEST_OU" -BackgroundColor Black -ForegroundColor Green

	$GROUPS = @()
	$xmlConfigs.config.groups.group | ForEach-Object {
		$GROUPS += $_.name
		Write-host "Processing Group => $($_.name)" -BackgroundColor Black -ForegroundColor Green
	}

	if ($ROOT -eq "") {Write-Error -Message "config.xml missing AD Root information." -ErrorAction Stop}
	if ($SERVER -eq "") {Write-Error -Message "config.xml missing AD Server information." -ErrorAction Stop}
	if (-Not $GROUPS.length) {Write-Error -Message "config.xml missing Group information." -ErrorAction Stop}
	if ($OUTPUT_PATH -eq "") {Write-Error -Message "config.xml missing Output Path information." -ErrorAction Stop}
	if ($LATEST_OU -eq "") {Write-Error -Message "config.xml missing Latest OU information." -ErrorAction Stop}
} catch {
	Write-host "Failure trying to read config.xml." -BackgroundColor Black -ForegroundColor Red
	Write-host "$_" -BackgroundColor Black -ForegroundColor Red
	Write-host "Aborting..." -BackgroundColor Black -ForegroundColor Red
	Exit
}

#
# Set Environment
#
Set-Location $env:USERPROFILE
Push-Location $env:USERPROFILE

#
# Set PS Provider - Active Directory
#
$ADConnection=New-PSDrive -Name ADDrive -PSProvider ActiveDirectory -Root $ROOT -Server $SERVER -ErrorAction Ignore
if ($ADConnection.Name -eq 'ADDrive') {
	if ($ADConnection.Name -eq 'ADDrive') {
		[System.Console]::Beep(600,500)
		Write-host ""
		Write-host "Connected to Active Directory" -BackgroundColor Black -ForegroundColor Yellow
	}
	Set-Location ADDrive:

# DO WORK

	#
	# Add users in specified OUs to specified Groups
	#

	# Parse OUs
	$OUs = @()
	$xmlConfigs.config.ous.ou | ForEach-Object {
		$OUs += $_.name
	}

	$results = @()
	$users = @()
	$dtimport = $null
	Write-host ""
	
	# Get all users in specified OUs
	foreach ($OU in $OUs) {
		Write-host "Processing OU => $OU" -BackgroundColor Black -ForegroundColor Green
		$users += Get-ADUser -Properties memberof -Filter * -Searchbase $OU
		if ($OU -match "DT Import") {
			$dtimport = $OU
			Write-host "DT Import OU specified, will attempt to move users from $dtimport" -BackgroundColor Black -ForegroundColor Yellow
		}
	}
	if ($dtimport -eq $null) {
		Write-host "DT Import OU not specified, will not attempt to move users" -BackgroundColor Black -ForegroundColor Yellow
	}
	# Parse groups information for collected users
	foreach ($user in $users) {
		$user_groups = $user.memberof -join ';'
		$results += New-Object psObject -Property @{'User'=$user.SamAccountName;'Groups'=$user_groups}
	}
	Write-host ""

	# Filter for users that are not part of the specified groups
	$users_to_add = $results | Where-Object { $_.groups -notmatch $GROUPS }# | Select-Object user
	
	# Add users to specified groups that are not already part of the group
	$output = @()
	foreach ($group in $GROUPS) {
		$count = 0
		foreach ($user in $users_to_add) {
			if (-Not ($($user.Groups) -match "$group")) {
				Add-ADGroupMember -Identity $group -Members $($user.User)
				$count++
				$date_time = Get-Date
				$output += "$date_time : Added user $($user.User) to group $group"
			}
		}
		Write-host "Added $count users to group: $group" -BackgroundColor Black -ForegroundColor Yellow
	}

	#
	# Move all users from DT Import to latest student OU if DT Import is included in the parsed OUs
	#

	Write-host ""
	$dtimportusers = @()
	$latestOUstring = $LATEST_OU.substring($LATEST_OU.indexOf("=")+1,($LATEST_OU.indexOf(",")-$LATEST_OU.indexOf("=")-1));
	if (-Not $($dtimport -eq $null)) {

		# Grab users in DT Import
		Write-host "Grabbing users from OU => $dtimport" -BackgroundColor Black -ForegroundColor Green
		$dtimportusers += Get-ADUser -Properties memberof -Filter * -Searchbase $dtimport

		# Add "Student" company to DT Import users
		Write-host ""
		Write-host "Modifying users' company to `"Student`"" -BackgroundColor Black -ForegroundColor Yellow
		foreach ($user in $dtimportusers) {
			Write-host "`"Student`" company added to user $($user.SamAccountName)" -BackgroundColor Black -ForegroundColor Green
			Set-ADUser -Identity $user.SamAccountName -Company "Student"
			$output += "$date_time : Added `"Student`" company to user $($user.SamAccountName)"
		}

		# Move DT Import users to latest Student OU
		Write-host ""
		Write-host "Moving users from $dtimport to $LATEST_OU" -BackgroundColor Black -ForegroundColor Yellow
		foreach ($user in $dtimportusers) {
			Write-host "$($user.SamAccountName) moved to $LATEST_OU" -BackgroundColor Black -ForegroundColor Green
			Set-ADUser -Identity $user.SamAccountName -Description $latestOUstring
			Move-ADObject -Identity $user -TargetPath $LATEST_OU
			$output += "$date_time : Moved $($user.SamAccountName) to $LATEST_OU and updated description"
		}
		
	}

	Write-host ""
	Write-host "Saving modified users to $OUTPUT_PATH" -BackgroundColor Black -ForegroundColor Yellow
	$date_time = Get-Date
	Add-Content -path $OUTPUT_PATH -value "`n"
	Add-Content -path $OUTPUT_PATH -value "########################################"
	Add-Content -path $OUTPUT_PATH -value "##   NEW RUN AT $date_time   ##"
	Add-Content -path $OUTPUT_PATH -value "########################################"
	if (-Not $output.length) {
		Add-Content -path $OUTPUT_PATH -value "No users missing group(s) { $($GROUPS -join ", ") }"
		Add-Content -path $OUTPUT_PATH -value "No users moved from DT Import"
	} else {
		$output | foreach { Add-Content -path $OUTPUT_PATH -value $_ }
	}

# STOP WORK

	Set-Location $configPath
	Push-Location $configPath
	Remove-PSDrive -Name ADDrive
} else {
	Write-host "`nUnable to Connect to Active Directory" -BackgroundColor Black -ForegroundColor Red
	[System.Console]::Beep(600,500)
}