# Usage of AD Find All In Organization powershell script

## Description

This script crawls AD OU's and saves all users who are a member of
a specified group to a csv file in the same directory. AD configuration can be specified
in the accompanying config.xml file. Exports firstname, lastname, fullname, email, and organization company to csv.

## Usage

Open powershell and navigate to the folder containing the "ad_find_all_in_organization.ps1" script.  
Modify the "config.xml" file as is appropriate.  
Run the "ad_find_all_in_organization.ps1" script.

## Table of Contents

- ad_find_all_in_organization.ps1 -- Powershell script
- config.xml -- AD configuration settings
- README.md -- This README

## Contributors  
Aaron Johnson  
Last Edit: 4/28/2022