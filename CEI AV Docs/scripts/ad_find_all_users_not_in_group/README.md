# Usage of AD Crawler powershell script

## Description

This script crawls user specified AD OU's and saves all users who are not a member of
a specified group to a csv file in the same directory. AD configuration can be specified
in the accompanying config.xml file.

## Usage

Open powershell and navigate to the folder containing the "ad_crawler.ps1" script.  
Modify the "config.xml" file as is appropriate.  
Run the "ad_crawler.ps1" script.

## Table of Contents

- ad_crawler.ps1 -- Powershell script
- config.xml -- AD configuration settings
- README.md -- This README

## Contributors  
Aaron Johnson  
Last Edit: 3/25/2022