#
# Import AD Module and Commands
#
Import-Module ActiveDirectory
Get-Command -Module ActiveDirectory
Clear-Host

#
# Setup Config Vars
#
try {
	$configPath = Get-Location
	Write-host "Attempting to read config from $configPath\config.xml" -BackgroundColor Black -ForegroundColor Yellow
	[xml]$xmlConfigs = Get-Content -Path "$configPath\config.xml"

	$ROOT = $xmlConfigs.config.root
	$SERVER = $xmlConfigs.config.server
	$MISSING_GROUP = $xmlConfigs.config.missing
	$OUTPUT_PATH = "$configPath" + $xmlConfigs.config.output

	Write-host "Loaded the following config..." -BackgroundColor Black -ForegroundColor Green
	Write-host "AD Root: $ROOT" -BackgroundColor Black -ForegroundColor Green
	Write-host "AD Server: $SERVER" -BackgroundColor Black -ForegroundColor Green
	Write-host "Missing Group: $MISSING_GROUP" -BackgroundColor Black -ForegroundColor Green
	Write-host "Output path: $OUTPUT_PATH" -BackgroundColor Black -ForegroundColor Green

	if ($ROOT -eq "") {Write-Error -Message "config.xml missing AD Root information." -ErrorAction Stop}
	if ($SERVER -eq "") {Write-Error -Message "config.xml missing AD Server information." -ErrorAction Stop}
	if ($MISSING_GROUP -eq "") {Write-Error -Message "config.xml missing Missing Group information." -ErrorAction Stop}
	if ($OUTPUT_PATH -eq "") {Write-Error -Message "config.xml missing Output Path information." -ErrorAction Stop}
} catch {
	Write-host "Failure trying to read config.xml." -BackgroundColor Black -ForegroundColor Red
	Write-host "$_" -BackgroundColor Black -ForegroundColor Red
	Write-host "Aborting..." -BackgroundColor Black -ForegroundColor Red
	Exit
}

#
# Set Environment
#
Set-Location $env:USERPROFILE
Push-Location $env:USERPROFILE

#
# Set PS Provider - Active Directory
#
$ADConnection=New-PSDrive -Name ADDrive -PSProvider ActiveDirectory -Root $ROOT -Server $SERVER -ErrorAction Ignore
if ($ADConnection.Name -eq 'ADDrive') {
	if ($ADConnection.Name -eq 'ADDrive') {
		[System.Console]::Beep(600,500)
		Write-host ""
		Write-host "Connected to Active Directory" -BackgroundColor Black -ForegroundColor Yellow
	}
	Set-Location ADDrive:

# DO WORK
	$OUs = @()
	$xmlConfigs.config.ous.ou | ForEach-Object {
		$OUs += $_.name
	}

	$results = @()
	$users = @()
	Write-host ""
	foreach ($OU in $OUs) {
		Write-host "Processing OU => $OU" -BackgroundColor Black -ForegroundColor Green
		$users += Get-ADUser -Properties memberof -Filter * -Searchbase $OU
	}
	foreach ($user in $users) {
		$groups = $user.memberof -join ';'
		$results += New-Object psObject -Property @{'User'=$user.SamAccountName;'Groups'=$groups}
	}
	Write-host ""
	Write-host "Saving users to $OUTPUT_PATH" -BackgroundColor Black -ForegroundColor Yellow
	$results | Where-Object { $_.groups -notmatch $MISSING_GROUP } | Select-Object user | export-csv -path $OUTPUT_PATH
# STOP WORK

	Set-Location $configPath
	Push-Location $configPath
	Remove-PSDrive -Name ADDrive
} else {
	Write-host "`nUnable to Connect to Active Directory" -BackgroundColor Black -ForegroundColor Red
	[System.Console]::Beep(600,500)
}