# Installation of GalliumOS on an HP Chromebox G2

## Description

This documentation set covers every aspect of the installation of GalliumOS on a stock HP Chromebox G2 model. It covers the use and modification of helper scripts, what software and firmware are needed, how to access developer tools, how to install Gallium OS in both a dual boot and a standalone configuration, and how to restore the HP Chromebox G2 to factory settings from any potential state.

## Table of Contents

README.md -- This README

Documentation/  
	- Enable Developer Mode Chromebox G2.docx  
	- Install Dual Boot GalliumOS on Chromebox G2.docx  
	- Install Standalone GalliumOS on Chromebox G2.docx  
	- Remote Connect to GalliumOS on Chromebox G2.docx  
	- Restore Chromebox G2 to Factory Settings.docx  

Scripts/  
	- Optisign Daemon/  
	- optisign_alwayson.service  
	- optisign_alwayson_daemon.c  
	- chrx-install (chrx.org)  
	- firmware-util.sh (mrchromebox.tech/#fwscript)  

Software and Firmware/  
	- balenaEtcher-Setup-1.7.7.exe (balena.io/etcher)  
	- galliumos-3.1-kabylake.iso (galliumos.org)  
	- optisigns-5.1.1-x86_64.Applmage (https://support.optisigns.com/hc/en-us/articles/360034379573-How-to-install-OptiSigns-Player-on-Linux)  
	- stock-firmware-KENCH-20220223.rom  

## Disclaimer

Read each document in 'Documentation/' thoroughly in its entirety prior to attempting the steps. There is a real possibility that your device may become unusable by following this process. By following these processes, you are taking responsibility for the risk and cost of errors and damages.

Last Edit:  
Aaron Johnson  
3/8/2022