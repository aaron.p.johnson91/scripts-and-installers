/*
 Daemon structure from http://www.netzmafia.de/skripten/unix/linux-daemon-howto.html

 Fork off the parent
 Change file mode mask (unmask)
 Open any logs for writing
 Create a unique Session ID (SID)
 Change the current working directory to a safe place
 Close standard file descriptors
 Enter actual daemon code
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <libnotify/notify.h>

int main(void) {

	/* Our process id and session id */
	pid_t pid, sid;

	/* Fork off the parent process */
	pid = fork()
	if (pid < 0)
	{
		exit(EXIT_FAILURE);
	}
	/* If we got a good PID, then we can exit the parent process */
	if (pid > 0)
	{
	exit(EXIT_SUCCESS);
	}

	/* Change the file mode mask */
	umask(0);

	/* OPTIONAL: Open a log file for writing */

	/* Create a new SID for the child process */
	sid = setsid();
	if (sid < 0)
	{
		/* Log any failure */
		exit(EXIT_FAILURE);
	}

	/* Change the cirrent working directory */
	if ((chdir("/")) < 0)
	{
		/* Log any failure here */
		exit(EXIT_FAILURE);
	}

	/* Close out the standard file descriptors */
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	/* Daemon code starts here */

	/* Daemon specific initialization goes here */

	notify_init("Optisigns daemon test");
	NotifyNotification* n = notify_notification_new ("This is a test for the optisigns daemon", "this is the second string in the test", 0);
	notify_notification_set_timeout(n,10000); // 10 seconds
	if (!notify_notification_show(n,0))
	{
		std::cerr << "notification show has failed" << std::endl;
		return -1;
	}

	/* The Big Loop */
	while (1)
	{
		/* Check for active optisign process */
		

		/* Do some task here ... */
		sleep(30); /* Wait 30 seconds*/
	}

	exit(EXIT_SUCCESS);

} /* End of main */