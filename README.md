# Scripts and Installers

## Description

These are scripts and installers that have been developed for various reasons and are being uploaded here for reference.

## Table of Contents

README.md -- This README

Chromebox GalliumOS Install Docs/  
- Documentation/  
	- Enable Developer Mode Chromebox G2.docx  
	- Install Dual Boot GalliumOS on Chromebox G2.docx  
	- Install Standalone GalliumOS on Chromebox G2.docx  
	- Remote Connect to GalliumOS on Chromebox G2.docx  
	- Restore Chromebox G2 to Factory Settings.docx  
- Scripts/  
	- Optisign Daemon/  
		- optisign_alwayson.service  
		- optisign_alwayson_daemon.c  
	- chrx-install (chrx.org)  
	- firmware-util.sh (mrchromebox.tech/#fwscript)  
- Software and Firmware/  
	- balenaEtcher-Setup-1.7.7.exe (balena.io/etcher)  
	- galliumos-3.1-kabylake.iso (galliumos.org)  
	- optisigns-5.1.1-x86_64.Applmage (https://support.optisigns.com/hc/en-us/articles/360034379573-How-to-install-OptiSigns-Player-on-Linux)  
	- stock-firmware-KENCH-20220223.rom  
- README.md  

scripts/  
- add_ad_group/  
	- add_ad_group.ps1  
	- config.xml (not included in this repo for security but required by script)  
	- README.md  
- check_missing_ad_group/  
	- ad_crawler.ps1
	- config.xml
	- README.md

Last Edit:  
Aaron Johnson  
4/26/2022